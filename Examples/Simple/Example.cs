using System;

namespace Iodynis.Libraries.Binding.Example
{
    public class Example
    {
        static void Main(string[] args)
        {
            ClassA a1 = new ClassA("one");
            ClassA a2 = new ClassA("two");
            ClassA a3 = new ClassA("three");

            a1.B1.C1.String = "initial";

            // Two-way binding
            //PropertyBind propertyBind = Binder
            //    .Property(a1, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
            //    //.Handle((_sender, _propertyValue) =>
            //    //{
            //    //    if (_propertyValue != null)
            //    //    {
            //    //        Console.WriteLine($"Property value is {_propertyValue}.");
            //    //    }
            //    //    else
            //    //    {
            //    //        Console.WriteLine($"Property value is null.");
            //    //    }
            //    //})
            //    .Property(a2, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
            //    //.Handle((_sender, _propertyValue) =>
            //    //{
            //    //    if (_propertyValue != null)
            //    //    {
            //    //        Console.WriteLine($"Property value is {_propertyValue}.");
            //    //    }
            //    //    else
            //    //    {
            //    //        Console.WriteLine($"Property value is null.");
            //    //    }
            //    //})
            //    .BindAndActivate(PropertyBindMode.BIND_BOTHWARD);

            PropertyBind propertyBind = Binder
                .Property(a1, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
                .Property(a2, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
                .BindAndActivate(PropertyBindMode.BIND_BOTHWARD);

            EventBind<string> eventBind = Binder
                .Property(a1, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
                .BindAndActivate<string>(StringChanged);
                //.BindAndActivate((_sender, _value) =>
                //{
                //    Console.WriteLine($"Smth changed: {_value}");
                //});

            Console.WriteLine($"{propertyBind.Value1 ?? "null"} = {propertyBind.Value2 ?? "null"}");

            //BinderController eventController = Binder
            //    .Event(a1, "B1.C1", nameof(ClassC.StringChanged), new EventHandler<string>((_object, _propertyValue) =>
            //    {
            //        Console.WriteLine($"Property value is {_propertyValue}");
            //    })).
            //    .Finalize();

            //Console.WriteLine();
            //Console.WriteLine(propertyController.Debug());
            //Console.WriteLine(eventController.Debug());

            //propertyBind.Activate();
            //eventController.Activate();
            //Console.WriteLine();
            //Console.WriteLine(propertyController.Debug());
            //Console.WriteLine(eventController.Debug());

            a1.B1.C1.String = "nya";
            Console.WriteLine($"{propertyBind.Value1} = {propertyBind.Value2}");
            //Console.WriteLine();
            //Console.WriteLine(propertyController.Debug());
            //Console.WriteLine(eventController.Debug());

            ClassB b1 = new ClassB("custom");
            Console.WriteLine($"{propertyBind.Value1} = {propertyBind.Value2}");
            b1.C1.String = "wtf";
            Console.WriteLine($"{propertyBind.Value1} = {propertyBind.Value2}");
            a1.B1 = b1;
            Console.WriteLine($"{propertyBind.Value1} = {propertyBind.Value2}");
            a1.B1 = null;
            Console.WriteLine($"{propertyBind.Value1} = {propertyBind.Value2}");
            a1.B1 = b1;
            Console.WriteLine($"{propertyBind.Value1} = {propertyBind.Value2}");
            //Console.WriteLine();
            //Console.WriteLine(propertyController.Debug());
            //Console.WriteLine(eventController.Debug());

            if (String.Equals(a1.B1.C1.String, a2.B1.C1.String))
            {
                throw new Exception();
            }
        }
        private static void StringChanged(object sender, string value)
        {
            Console.WriteLine($"String is {value}");
        }
        private static void ObjectChanged(object sender, object value)
        {
            Console.WriteLine($"Object is {value}");
        }
    }
}
