﻿namespace Iodynis.Libraries.Binding.Example
{
    public class ClassC : Class
    {
        public ClassC()
        {
            ;
        }
        public ClassC(string name)
            : base(name)
        {
            ;
        }
        public override string ToString()
        {
            return $"ClassC {Name}";
        }
    }
}
