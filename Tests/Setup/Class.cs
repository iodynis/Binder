﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Iodynis.Libraries.Binding.Test
{
    public class Class
    {
        public virtual event PropertyChangingEventHandler PropertyChanging;
        private void NotifyPropertyChanging([CallerMemberName] string propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }
        public virtual event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public delegate void CustomDelegate(bool boolean , int integer, string @string, object @object);

        public event Action BooleanChangedAction;
        public event CustomDelegate BooleanChangedCustomDelegate;
        public event EventHandler BooleanChangedEventHadlerGeneral;
        public event EventHandler<bool> BooleanChangedEventHadlerSpecific;
        public event EventHandler<Class, bool> BooleanChangedEventHadlerCustom;

        private bool _Boolean;
        public event EventHandler<bool> BooleanChanged;
        public event EventHandler<Class, bool> BooleanAltered;
        public bool Boolean
        {
            get
            {
                return _Boolean;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Boolean;
                _Boolean = value;
                BooleanChanged?.Invoke(this, value);
                BooleanAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private char _Character;
        public event EventHandler<char> CharacterChanged;
        public event EventHandler<Class, char> CharacterAltered;
        public char Character
        {
            get
            {
                return _Character;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Character;
                _Character = value;
                CharacterChanged?.Invoke(this, value);
                CharacterAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private short _Short;
        public event EventHandler<short> ShortChanged;
        public event EventHandler<Class, short> ShortAltered;
        public short Short
        {
            get
            {
                return _Short;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Short;
                _Short = value;
                ShortChanged?.Invoke(this, value);
                ShortAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private int _Integer;
        public event EventHandler<int> IntegerChanged;
        public event EventHandler<Class, int> IntegerAltered;
        public int Integer
        {
            get
            {
                return _Integer;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Integer;
                _Integer = value;
                IntegerChanged?.Invoke(this, value);
                IntegerAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private long _Long;
        public event EventHandler<long> LongChanged;
        public event EventHandler<Class, long> LongAltered;
        public long Long
        {
            get
            {
                return _Long;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Long;
                _Float = value;
                LongChanged?.Invoke(this, value);
                LongAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private float _Float;
        public event EventHandler<float> FloatChanged;
        public event EventHandler<Class, float> FloatAltered;
        public float Float
        {
            get
            {
                return _Float;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Float;
                _Float = value;
                FloatChanged?.Invoke(this, value);
                FloatAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private double _Double;
        public event EventHandler<double> DoubleChanged;
        public event EventHandler<Class, double> DoubleAltered;
        public double Double
        {
            get
            {
                return _Double;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Double;
                _Double = value;
                DoubleChanged?.Invoke(this, value);
                DoubleAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private string _String;
        public event EventHandler<string> StringChanged;
        public event EventHandler<Class, string> StringAltered;
        public string String
        {
            get
            {
                return _String;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _String;
                _String = value;
                StringChanged?.Invoke(this, value);
                StringAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private object _Object;
        public event EventHandler<object> ObjectChanged;
        public event EventHandler<Class, object> ObjectAltered;
        public object Object
        {
            get
            {
                return _String;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _Object;
                _Object = value;
                ObjectChanged?.Invoke(this, value);
                ObjectAltered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }

        public Class()
        {
            ;
        }
    }
}
