﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iodynis.Libraries.Binding.Test
{
    [TestClass]
    public class BinderTest
    {
        [TestMethod]
        public void BinderPropertyBindTest()
        {
            ClassA a1 = new ClassA();
            ClassA a2 = new ClassA();
            ClassA a3 = new ClassA();

            // Two-way binding
            Binder
                .Property(a1, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String))
                .Property(a2, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String))
                .BindAndActivate();

            a1.B1.C1.String = "nya";
            Assert.AreEqual(a1.B1.C1.String, a2.B1.C1.String);
            a2.B1 = new ClassB();
            Assert.AreEqual(a1.B1.C1.String, a2.B1.C1.String);

            // One-way binding

            //
        }
        [TestMethod]
        public void BinderEventBindTest()
        {
            ;
        }
    }
}
