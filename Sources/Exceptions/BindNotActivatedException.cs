﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// THe excaption that is thrown when the bind is expected to be activated but it is not.
    /// </summary>
    public class BindNotActivatedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BindNotActivatedException"/> class.
        /// </summary>
        public BindNotActivatedException()
            : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindNotActivatedException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public BindNotActivatedException(string message)
            : base(message) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindNotActivatedException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public BindNotActivatedException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
