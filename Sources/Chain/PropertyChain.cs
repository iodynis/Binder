﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Binding
{
    internal class PropertyChain
    {
        internal enum Modes
        {
            FAILFAST,
            LIVELONG,
        }
        internal Modes Mode { get; set; } = Modes.FAILFAST;

        internal bool IsActive
        {
            get
            {
                return Root.IsActive;
            }
        }
        internal bool IsConnected
        {
            get
            {
                return End.IsActive;
            }
        }
        private bool WasEndActivate;

        #region Events
        internal event Action PropertyValueChanged;
        internal event Action PropertyPathBroke;
        internal event Action PropertyPathRestored;
        internal event Action PropertyPathChanged;
        internal void TriggerPropertyValueChanged()
        {
            PropertyValueChanged?.Invoke();
        }
        internal void TriggerPropertyChainChanged()
        {
            if (End.IsActive)
            {
                // Property path has changed
                if (WasEndActivate)
                {
                    PropertyPathChanged?.Invoke();
                    // Property has changed too
                    //PropertyValueChanged?.Invoke();
                }
                // Property path was restored
                else
                {
                    PropertyPathRestored?.Invoke();
                    // Property has changed too
                    //PropertyValueChanged?.Invoke();
                }
            }
            else
            {
                // Property path was broken
                if (WasEndActivate)
                {
                    PropertyPathBroke?.Invoke();
                    // Property has changed too
                    //PropertyValueChanged?.Invoke();
                }
                // Property path remains broken
                else
                {
                    ;
                }
            }
            WasEndActivate = End.IsActive;
        }
        #endregion

        private PropertyChainLink Root { get; }
        private PropertyChainLink End { get; }

        internal object PropertyValue
        {
            get
            {
                // Check if this chain piece is activated
                if (!IsConnected)
                {
                    throw new Exception("Cannot get Object value because chain is inactive.");
                }

                // Return the value
                return End.PropertyValue;
                //return End.IsActive ? End.PropertyValue : PropertyValueDefault;
            }
            set
            {
                // Check if this chain piece is activated
                if (!IsConnected)
                {
                    throw new Exception("Cannot set Object value because chain is inactive.");
                }

                // Set value to the back field so that
                End.PropertyValue = value;
            }
        }
        internal object PropertyObject
        {
            get
            {
                // Check if this chain piece is activated
                if (!IsConnected)
                {
                    throw new Exception("Cannot get Object value because chain is inactive.");
                }

                // Return the value
                return End.Object;
            }
        }
        //private object PropertyValueDefault { get; set; }

        private string PropertyPath { get; }
        private List<string> PropertyNames { get; }
        private string EventPath { get; }
        private List<string> EventNames { get; }

        internal PropertyChain(object rootObject, string[] propertyPath, string[] eventPath)//, object propertyValueDefault)
        {
            if (rootObject == null)
            {
                throw new ArgumentNullException(nameof(rootObject), "Root object cannot be null.");
            }
            if (propertyPath == null)
            {
                throw new ArgumentNullException(nameof(propertyPath), "Property path cannot be null.");
            }

            // As property path can contain property naems as well as property partial paths,
            // join all pieces together in one full path and then split into names.
            PropertyPath = String.Join(".", propertyPath);
            PropertyNames = new List<string>(PropertyPath.Split('.'));
            EventPath = eventPath != null ? String.Join(".", eventPath) : null;
            EventNames = EventPath != null ? new List<string>(EventPath.Split('.')) : null;
            //PropertyValueDefault = propertyValueDefault;

            if (EventNames != null && EventNames.Count != PropertyNames.Count)
            {
                throw new ArgumentException($"Event names must be of the same count as property names. {EventNames.Count} event names were provided. Property path {PropertyPath} contains {PropertyNames.Count} property names.", nameof(EventPath));
            }

            Root = new PropertyChainLink(this, rootObject, PropertyNames[0]);
            End = Root;

            for (int index = 1; index < PropertyNames.Count; index++)
            {
                PropertyChainLink link = new PropertyChainLink(this, PropertyNames[index]);
                End.Add(link);
                End = link;
            }

            if (EventNames != null)
            {
                Root.SetEventNames(EventNames);
            }
        }
        //internal void SetEventNames(params string[] eventNames)
        //{
        //    if (eventNames == null)
        //    {
        //        throw new ArgumentNullException(nameof(eventNames), "Event names cannot be null.");
        //    }
        //    if (eventNames.Length != PropertyNames.Count)
        //    {
        //        throw new ArgumentException($"Event names must be of the same count as property names. {eventNames.Length} event names were provided. Property path {PropertyPath} contains {PropertyNames.Count} property names.", nameof(eventNames));
        //    }

        //    Root.SetEventNames(eventNames);
        //}
        internal void Initialize()
        {
            Root.Initialize();
        }
        internal void Activate(PropertyChainLink linkToStartAt = null)
        {
            PropertyChainLink link = linkToStartAt ?? Root;
            while (link != null)
            {
                if (!link.IsActive)
                {
                    link.Activate();
                }
                link = link.Next;
            }

            // Chain has changed
            TriggerPropertyChainChanged();
        }
        internal void Deactivate(PropertyChainLink linkToStopAt = null)
        {
            PropertyChainLink link = End;
            while (link != linkToStopAt)
            {
                if (link.IsActive)
                {
                    link.Deactivate();
                }
                link = link.Previous;
            }
            if (link.IsActive)
            {
                link.Deactivate();
            }
        }
        internal void Reactivate(PropertyChainLink link)
        {
            if (link.IsEnd)
            {
                return;
            }

            // Deactivate the further chain
            Deactivate(link);
            // Activate it again
            Activate(link);
        }
        internal string Debug()
        {
            return Root.Debug();
        }
        //internal void Deactivate(BinderPropertyChainLink linkToStopAt)
        //{
        //    if (this == linkToStopAt)
        //    {
        //        return;
        //    }

        //    // Unsubscribe from the event
        //    if (EventHandler != null)
        //    {
        //        EventInfo.RemoveEventHandler(Object, EventHandler);
        //    }

        //    IsActive = false;

        //    Previous?.Deactivate();
        //}
    }
}
