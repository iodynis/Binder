﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Iodynis.Libraries.Binding
{
    internal class PropertyChainLink
    {
        internal PropertyChain Chain { get; }
        internal PropertyChainLink Next { get; private set; }
        internal PropertyChainLink Previous { get; private set; }
        internal bool IsRoot { get; private set; }
        internal bool IsEnd { get; private set; }

        // Object
        internal Type ObjectType { get; private set; }
        internal string ObjectName { get; private set; }
        internal object Object { get; private set; }

        // Property
        internal string PropertyName { get; }
        internal Type PropertyType { get; private set; }
        internal PropertyInfo PropertyInfo { get; private set; }
        internal FieldInfo FieldInfo { get; private set; }
        internal bool PropertyValueIsBeingSet = false;
        internal object _PropertyValue;
        internal object PropertyValue
        {
            get
            {
                return _PropertyValue;
            }
            set
            {
                if (Next != null)
                {
                    throw new Exception("Property value cannot be set for the non-end property chain link.");
                }

                try
                {
                    PropertyValueIsBeingSet = true;
                    PropertyInfo.SetValue(Object, value);
                    _PropertyValue = value;
                }
                catch
                {
                    if (Chain.Mode == PropertyChain.Modes.FAILFAST)
                    {
                        throw new Exception($"Binder failed to set value of {_PropertyValue} to {ObjectName}.{PropertyName}.");
                    }
                }
                finally
                {
                    PropertyValueIsBeingSet = false;
                }
            }
        }
        //public FieldInfo FieldInfo { get; private set; }

        // Event
        private string EventName { get; set; }
        private EventInfo EventInfo { get; set; }
        private Delegate EventHandler { get; set; }
        private Type[] EventPropertyValueChangedMethodArguments { get; set; }
        private MethodInfo EventPropertyValueChangedMethodInfo { get; set; }
        private bool IsThereAnEventHereOrUpTheChain { get; set; } = false;

        internal bool IsInitialized { get; private set; } = false;
        internal bool IsActive { get; private set; } = false;

        internal PropertyChainLink(PropertyChain chain, object @object, string propertyName)
            : this(chain, propertyName)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object), "Object cannot be null.");
            }

            Object = @object;
        }
        internal PropertyChainLink(PropertyChain chain, string propertyName)
        {
            if (chain == null)
            {
                throw new ArgumentNullException(nameof(chain), "Chain cannot be null.");
            }
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName), "Propery name cannot be null.");
            }

            Chain = chain;
            PropertyName = propertyName;
        }
        internal void SetEventNames(List<string> eventNames, int index = 0)
        {
            EventName = eventNames[index];
            Next?.SetEventNames(eventNames, index + 1);
        }
        internal void Add(PropertyChainLink link)
        {
            if (Next != null)
            {
                Next.Add(link);
            }
            else
            {
                Next = link;
                link.Previous = this;
            }
        }
        internal void Initialize()
        {
            if (IsInitialized)
            {
                throw new Exception("Chain link is already initialized.");
            }

            if (Object == null && Previous == null)
            {
                throw new Exception("Root cannot be null.");
            }

            IsRoot = Previous == null;
            IsEnd = Next == null;
            ObjectType = IsRoot ? Object.GetType() : Previous.PropertyType;
            ObjectName = IsRoot ? ObjectType.Name : Previous.PropertyName;
            PropertyInfo = ObjectType.GetProperty(PropertyName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);

            if (PropertyInfo == null)
            {
                FieldInfo = ObjectType.GetField(PropertyName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
                if (FieldInfo == null)
                {
                    throw new Exception($"{ObjectType} does not have neither {PropertyName} property nor field.");
                }

                PropertyType = FieldInfo.FieldType;
            }
            else
            {
                PropertyType = PropertyInfo.PropertyType;

                // Try default event name
                if (EventName == null)
                {
                    string eventName = PropertyName + "Changed";
                    EventInfo eventInfo = ObjectType.GetEvent(eventName);
                    if (eventInfo != null)
                    {
                        EventName = eventName;
                        EventInfo = eventInfo;
                    }
                }
                // Try custom default event name
                if (EventName == null)
                {
                    string eventName = PropertyName + "Altered";
                    EventInfo eventInfo = ObjectType.GetEvent(eventName);
                    if (eventInfo != null)
                    {
                        EventName = eventName;
                        EventInfo = eventInfo;
                    }
                }
                // Try INotifyPropertyChanged interface event name
                if (EventName == null)
                {
                    string eventName = "PropertyChanged";
                    EventInfo eventInfo = ObjectType.GetEvent(eventName);
                    if (eventInfo != null)
                    {
                        EventName = eventName;
                        EventInfo = eventInfo;
                    }
                }
                if (EventInfo != null)
                {
                    if (EventName == "PropertyChanged")
                    {
                        // Convert (object, PropertyChangedEventHandler) delegate to whatever event handler wants
                        EventPropertyValueChangedMethodArguments = new [] { typeof(object), typeof(PropertyChangedEventArgs) };
                        EventPropertyValueChangedMethodInfo = GetType().GetMethod(nameof(EventPropertyValueChanged_PropertyChangedHandler), BindingFlags.NonPublic | BindingFlags.Instance);//, null, EventPropertyValueChangedMethodArguments, null);
                    }
                    else if (EventInfo.EventHandlerType.GenericTypeArguments.Length == 0)
                    {
                        // Convert (object, EventArgs) delegate to whatever event handler wants
                        EventPropertyValueChangedMethodArguments = new [] { typeof(object), typeof(EventArgs) };
                        EventPropertyValueChangedMethodInfo = GetType().GetMethod(nameof(EventPropertyValueChanged_EventArgsHandler), BindingFlags.NonPublic | BindingFlags.Instance);//, null, EventPropertyValueChangedMethodArguments, null);
                    }
                    else if (EventInfo.EventHandlerType.GenericTypeArguments.Length == 1)
                    {
                        if (!EventInfo.EventHandlerType.GenericTypeArguments[0].IsValueType)
                        {
                            // Convert (object, object) delegate to whatever event handler wants
                            EventPropertyValueChangedMethodArguments = new [] { typeof(object), typeof(object) };
                            EventPropertyValueChangedMethodInfo = GetType().GetMethod(nameof(EventPropertyValueChanged_ObjectHandler), BindingFlags.NonPublic | BindingFlags.Instance);//, null, EventPropertyValueChangedMethodArguments, null);
                        }
                        else
                        {
                            // Convert (object, T) delegate to whatever event handler wants
                            EventPropertyValueChangedMethodArguments = new [] { typeof(object), EventInfo.EventHandlerType.GenericTypeArguments[0] };
                            // Get the type that we want in put into generic method
                            Type valueType = EventInfo.EventHandlerType.GenericTypeArguments[0];
                            // Get generic method
                            MethodInfo propertyValueChangedMethodInfoGeneric = GetType().GetMethod(nameof(EventPropertyValueChanged_GenericHandler), BindingFlags.NonPublic | BindingFlags.Instance);//, null, EventPropertyValueChangedMethodArguments, null);
                            //MethodInfo propertyValueChangedMethodInfoGeneric = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).First(_methodInfo =>
                            //{
                            //    return _methodInfo.IsGenericMethod && _methodInfo.Name == nameof(EventPropertyValueChangedHandlerGeneric);
                            //});
                            // Make specific method
                            EventPropertyValueChangedMethodInfo = propertyValueChangedMethodInfoGeneric.MakeGenericMethod(new [] { valueType });
                        }
                    }
                    else
                    {
                        throw new Exception($"Event {EventName} is not of a supported type.");
                    }
                    // Create delegate with appropriate signature
                    EventHandler = Delegate.CreateDelegate(EventInfo.EventHandlerType, this, EventPropertyValueChangedMethodInfo);
                }
            }

            IsThereAnEventHereOrUpTheChain = EventInfo != null || (Previous != null && Previous.IsThereAnEventHereOrUpTheChain);
            IsInitialized = true;
            Next?.Initialize();
        }
        internal void Activate()//bool skipEventBinding = false)
        {
            //if (EventInfo == null && !skipEventBinding)
            //{
            //    throw new Exception($"Class {ObjectName} has no event for tracking the changes of {PropertyName} property: no {PropertyName}Changed, {PropertyName}Changed<T> or PropertyChanged events were found in the {ObjectType.FullName} class.");
            //}
            if (Object == null)
            {
                return;
            }

            // Subscribe to the event
            if (EventHandler != null)
            {
                EventInfo.AddEventHandler(Object, EventHandler);
            }

            // Get the value
            if (PropertyInfo != null)
            {
                _PropertyValue = PropertyInfo.GetValue(Object);
            }
            else
            {
                _PropertyValue = FieldInfo.GetValue(Object);
            }

            // Update the next link object
            if (!IsEnd)
            {
                // If value is null -- cannot activate further for now
                if (_PropertyValue == null)
                {
                    // Moreover, if there is no event that can change the situation
                    if (!IsThereAnEventHereOrUpTheChain)
                    {
                        throw new Exception($"{ObjectName} neither has a {PropertyName}Changed, {PropertyName}Changed<T> or PropertyChanged event nor its {PropertyName} property is set to a value.");
                    }
                    // Wait for some event to change the situation
                    return;
                }
                Next.Object = _PropertyValue;
            }
            else
            {
                ;
            }

            IsActive = true;

            //if (!IsEnd)
            //{
            //    Next.Activate();
            //}
            //else
            //{
            //    Chain.TriggerPropertyValueChanged();
            //}
        }
        internal void Deactivate()
        {
            // Nullify data
            if (!IsEnd)
            {
                Next.Object = null;
            }
            if (!IsRoot)
            {
                _PropertyValue = null;
            }

            // Unsubscribe from the event
            if (EventHandler != null)
            {
                EventInfo.RemoveEventHandler(Object, EventHandler);
            }

            IsActive = false;
        }
        private void EventPropertyValueChanged_GenericHandler<T>(object @object, T propertyValue)
        {
            if (PropertyValueIsBeingSet) return;

            _PropertyValue = propertyValue;

            if (IsEnd)
            {
                Chain.TriggerPropertyValueChanged();
            }
            else
            {
                Chain.Reactivate(this);
            }
        }
        private void EventPropertyValueChanged_ObjectHandler(object @object, object propertyValue)
        {
            if (PropertyValueIsBeingSet) return;

            _PropertyValue = propertyValue;

            if (IsEnd)
            {
                Chain.TriggerPropertyValueChanged();
            }
            else
            {
                Chain.Reactivate(this);
            }
        }
        private void EventPropertyValueChanged_EventArgsHandler(object @object, EventArgs eventArgs)
        {
            if (PropertyValueIsBeingSet) return;

            _PropertyValue = PropertyInfo.GetValue(Object);

            if (IsEnd)
            {
                Chain.TriggerPropertyValueChanged();
            }
            else
            {
                Chain.Reactivate(this);
            }
        }
        private void EventPropertyValueChanged_PropertyChangedHandler(object @object, PropertyChangedEventArgs eventArgs)
        {
            if (PropertyValueIsBeingSet) return;

            // Check this is the right property
            if (PropertyName != eventArgs.PropertyName)
            {
                return;
            }
            // Update the property chached value
            _PropertyValue = PropertyInfo.GetValue(Object);

            if (IsEnd)
            {
                Chain.TriggerPropertyValueChanged();
            }
            else
            {
                Chain.Reactivate(this);
            }
        }
        internal string Debug()
        {
            return
                (IsRoot ? "    " + ObjectName + Environment.NewLine : "") +
                "    " + PropertyName + " = " + (PropertyValue != null ? PropertyValue.ToString() : "null") +
                (IsInitialized ? "      Initialized" : "") +
                (IsActive ? " Active" : "") +
                Environment.NewLine +
                (IsEnd ? (PropertyValue != null ? "    " + PropertyValue.ToString() : "    null") : Next?.Debug());
        }
        //private void Handle()
        //{
        //    bool isActive = IsActive;

        //    // If in the middle of the chain
        //    if (Next != null)
        //    {
        //        Reactivate();
        //        // If chain reactivation succeedes
        //        if (Reactivate())
        //        {
        //            if (Equals(EndValuePrevious, EndValue))
        //            {
        //                return;
        //            }
        //            EndValuePrevious = EndValue;

        //            // Raise event with new object value
        //            Chain.End.PropertyValueChanged?.Invoke(BinderPopertyChainState.ACTIVATED, EndValue);
        //        }
        //        // If chain reactivation fails
        //        else
        //        {
        //            // Raise event with null
        //            Chain.End.PropertyValueChanged?.Invoke(isActive ? BinderPopertyChainState.DEACTIVATED : BinderPopertyChainState.INACTIVE, null);
        //        }
        //    }
        //    // If at the end of the chain
        //    else
        //    {
        //        EndValuePrevious = _PropertyInfo.GetValue(@object);
        //        // Raise event with new object value
        //        Chain.End.PropertyValueChanged?.Invoke(BinderPopertyChainState.ACTIVE, EndValuePrevious);
        //    }
        //}
    }
}
