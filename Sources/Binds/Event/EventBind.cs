﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// Object to control the event bind.
    /// </summary>
    /// <typeparam name="T">Type of the binded property.</typeparam>
    public class EventBind<T> : Bind
    {
        internal Property Property;
        internal EventHandler<T> EventHandler;

        private bool IsActive = false;

        internal EventBind(Property property, EventHandler<T> eventHandler)
            : base()
        {
            Property = property;
            EventHandler = eventHandler;

            Property.Initialize();
        }
        private void Subscribe()
        {
            Property.Activated += TriggerInternal;
            Property.Reactivated += TriggerInternal;
            Property.Deactivated += TriggerInternal;
            Property.Changed += TriggerInternal;
        }
        private void Unsubscribe()
        {
            Property.Activated -= TriggerInternal;
            Property.Reactivated -= TriggerInternal;
            Property.Deactivated -= TriggerInternal;
            Property.Changed -= TriggerInternal;
        }
        /// <summary>
        /// Activate the bind.
        /// </summary>
        /// <returns>Itself.</returns>
        /// <exception cref="Exception">If the bind is already activated.</exception>
        public override Bind Activate()
        {
            if (IsActive)
            {
                throw new BindAlreadyActivatedException("Bind is active and thus cannot be activated.");
            }

            Property.Activate();
            Subscribe();
            IsActive = true;
            return this;
        }
        /// <summary>
        /// Deactivate the bind.
        /// </summary>
        /// <returns>Itself.</returns>
        /// <exception cref="Exception">If the bind is not activated.</exception>
        public override Bind Deactivate()
        {
            if (!IsActive)
            {
                throw new BindNotActivatedException("Bind is not active and thus cannot be deactivated.");
            }

            Unsubscribe();
            Property.Deactivate();
            IsActive = false;
            return this;
        }
        /// <summary>
        /// Trigger an active bind.
        /// </summary>
        /// <returns>Itself.</returns>
        /// <exception cref="Exception">If the bind is not activated.</exception>
        public override Bind Trigger()
        {
            if (!IsActive)
            {
                throw new BindNotActivatedException("Bind is not active and thus cannot be triggered.");
            }

            TriggerInternal();
            return this;
        }
        private void TriggerInternal()
        {
            EventHandler.Invoke(Property.Object, (T)Property.Value);
        }
        //internal override string Debug()
        //{
        //    return "Event binding." + Environment.NewLine + Event.Debug() + Environment.NewLine + Property.Debug();
        //}
    }
    /// <summary>
    /// Object to control the event bind.
    /// </summary>
    public class EventBind : EventBind<object>
    {
        internal EventBind(Property property, EventHandler<object> eventHandler)
            : base(property, eventHandler)
        {
            ;
        }
    }
}
