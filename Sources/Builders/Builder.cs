﻿using System;

namespace Iodynis.Libraries.Binding
{
    internal class Builder
    {
        internal PropertyBindModePrecise Mode;
        internal object PropertyOneObject;
        internal string[] PropertyOnePathProperties;
        internal string[] PropertyOnePathEvents;
        /// <summary>
        /// Default value is used when the propery path is broken and therefore the property value itself cannot be determined.
        /// In case property is ok and its value is null -- null will be returned, default value will not be used.
        /// </summary>
        internal object PropertyOneValueDefault;
        internal PropertyConverter ConverterPropertyOneToPropertyTwo;
        //internal readonly List<EventHandler<dynamic>> PropertyOneHandlers = new List<EventHandler<dynamic>>();
        //internal bool PropertyOneIsUpdatedWhenBindedsPathIsInitialized;
        //internal bool PropertyOneIsUpdatedWhenBindedPathIsActivated;
        //internal bool PropertyOneIsUpdatedWhenBindedPathIsChanged;
        //internal bool PropertyOneIsUpdatedWithDefaultValueWhenBindedPathIsBroken;
        //internal bool PropertyOneIsUpdatedWhenBindedPropertyChanged;

        internal object PropertyTwoObject;
        internal string[] PropertyTwoPathProperties;
        internal string[] PropertyTwoPathEvents;
        /// <summary>
        /// Default value is used when the propery path is broken and therefore the property value itself cannot be determined.
        /// In case property is ok and its value is null -- null will be returned, default value will not be used.
        /// </summary>
        internal object PropertyTwoValueDefault;
        internal PropertyConverter ConverterPropertyTwoToPropertyOne;
        //internal readonly List<EventHandler<dynamic>> PropertyTwoHandlers = new List<EventHandler<dynamic>>();
        //internal bool PropertyTwoIsUpdatedWhenBindedsPathIsInitialized;
        //internal bool PropertyTwoIsUpdatedWhenBindedPathIsActivated;
        //internal bool PropertyTwoIsUpdatedWhenBindedPathIsChanged;
        //internal bool PropertyTwoIsUpdatedWithDefaultValueWhenBindedPathIsBroken;
        //internal bool PropertyTwoIsUpdatedWhenBindedPropertyChanged;

        internal PropertyBind BuildPropertyBind()
        {
            //// Check if the defaut values are converted to each other
            //if (PropertyOneValueDefault != null && PropertyTwoValueDefault != null &&
            //    (PropertyOneValueDefault.Equals(ConverterPropertyOneToPropertyTwo.Convert(PropertyTwoValueDefault)) || PropertyTwoValueDefault.Equals(ConverterPropertyTwoToPropertyOne.Convert(PropertyOneValueDefault))))
            //{
            //    throw new Exception("The properties default values are not equal.");
            //}

            Property propertyOne = new Property(PropertyOneObject, PropertyOnePathProperties, PropertyOnePathEvents, PropertyOneValueDefault);//, PropertyOneHandlers);
            Property propertyTwo = new Property(PropertyTwoObject, PropertyTwoPathProperties, PropertyTwoPathEvents, PropertyTwoValueDefault);//, PropertyTwoHandlers);
            return new PropertyBind(propertyOne, ConverterPropertyOneToPropertyTwo, propertyTwo, ConverterPropertyTwoToPropertyOne, Mode);
        }
        internal EventBind BuildEventBind(EventHandler<object> eventHandler)
        {
            Property property = new Property(PropertyOneObject, PropertyOnePathProperties, PropertyOnePathEvents, PropertyOneValueDefault);
            //Event @event = new Event(EventHandler);
            return new EventBind(property, eventHandler);
        }
        internal EventBind<T> BuildEventBind<T>(EventHandler<T> eventHandler)
        {
            Property property = new Property(PropertyOneObject, PropertyOnePathProperties, PropertyOnePathEvents, PropertyOneValueDefault);
            //Event @event = new Event(EventHandler);
            return new EventBind<T>(property, eventHandler);
        }
    }
}
