﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// The second phase builder for the event bind.
    /// </summary>
    public class BuilderEvent
    {
        private readonly Builder Builder;
        private bool isDone;

        internal BuilderEvent(Builder builder)
        {
            Builder = builder;
        }

        /// <summary>
        /// Finalize the binding.
        /// </summary>
        /// <returns>The bind.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public EventBind Bind(EventHandler<object> handler)
        {
            ThrowExceptionIfDone();
            isDone = true;

            return Builder.BuildEventBind(handler);
        }
        /// <summary>
        /// Finalize the binding.
        /// </summary>
        /// <returns>The bind.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public EventBind<T> Bind<T>(EventHandler<T> handler)
        {
            ThrowExceptionIfDone();
            isDone = true;

            return Builder.BuildEventBind(handler);
        }
        private void ThrowExceptionIfDone()
        {
            if (isDone)
            {
                throw new Exception("The builder is already finalized.");
            }
        }
    }
}
