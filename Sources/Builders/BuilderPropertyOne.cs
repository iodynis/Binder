﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// The first phase builder for the property or event bind.
    /// </summary>
    public class BuilderPropertyOne
    {
        private readonly Builder Builder;
        private bool isDone;

        internal BuilderPropertyOne(Builder builder)
        {
            Builder = builder;
        }
        /// <summary>
        /// A custom converter to use to convert this property value to the value of its bounded property.
        /// </summary>
        /// <param name="converter">Converter from this property to the binded one.</param>
        /// <returns>Itself.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyOne Converter(PropertyConverter converter)
        {
            ThrowExceptionIfDone();

            Builder.ConverterPropertyOneToPropertyTwo = converter;
            return this;
        }
        /// <summary>
        /// A custom converter to use to convert this property value to the value of its bounded property.
        /// </summary>
        /// <param name="converter">Converter from this property to the binded one.</param>
        /// <returns>Itself.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyOne Converter(Func<object, object> converter)
        {
            ThrowExceptionIfDone();

            Builder.ConverterPropertyOneToPropertyTwo = new PropertyConverter(converter);
            return this;
        }
        /// <summary>
        /// A custom converter to use to convert this property value to the value of its bounded property.
        /// </summary>
        /// <param name="converter">Converter from this property to the binded one.</param>
        /// <returns>Itself.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyOne Converter<T1, T2>(Func<T1, T2> converter) where T1 : class where T2 : class
        {
            ThrowExceptionIfDone();

            Builder.ConverterPropertyOneToPropertyTwo = new PropertyConverter(converter as Func<object, object>);
            return this;
        }
        /// <summary>
        /// Set the default property value to use when it is not connected to an actual property.
        /// </summary>
        /// <param name="propertyValueDefault">The default value for the property.</param>
        /// <returns>Itself.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyOne Default(object propertyValueDefault)
        {
            ThrowExceptionIfDone();

            Builder.PropertyOneValueDefault = propertyValueDefault;
            return this;
        }
        /// <summary>
        /// Set custom event names to use instead of [PropertyNameHere]Changed, [PropertyNameHere]Changed&lt;T&gt; and PropertyChanged. Use null values for automatical binding to [PropertyNameHere]Changed, [PropertyNameHere]Changed&lt;T&gt; and PropertyChanged.
        /// </summary>
        /// <param name="eventNames">Event names to use.</param>
        /// <returns>Itself.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyOne CustomEvents(params string[] eventNames)
        {
            ThrowExceptionIfDone();

            Builder.PropertyOnePathEvents = eventNames;
            return this;
        }
        /// <summary>
        /// Wrap the property for binding.
        /// </summary>
        /// <param name="object">Object that exists at the moment of binding.</param>
        /// <param name="propertyPath">Property full path, partial paths or names. For example, all these arrays represent the same property path: ["Settings.Size.Width"], ["Settings.Size", "Width"] and ["Settings", "Size", "Width"].</param>
        /// <returns>The next phase property bind builder.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyTwo Property(object @object, params string[] propertyPath)
        {
            ThrowExceptionIfDone();
            isDone = true;

            Builder.PropertyTwoObject = @object;
            Builder.PropertyTwoPathProperties = propertyPath;
            return new BuilderPropertyTwo(Builder);
        }

        /// <summary>
        /// Finalize the binding.
        /// </summary>
        /// <returns>The bind.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public EventBind Bind(EventHandler<object> handler)
        {
            ThrowExceptionIfDone();
            isDone = true;

            return Builder.BuildEventBind(handler);
        }
        /// <summary>
        /// Finalize the binding.
        /// </summary>
        /// <returns>The bind.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public EventBind<T> Bind<T>(EventHandler<T> handler)
        {
            ThrowExceptionIfDone();
            isDone = true;

            return Builder.BuildEventBind(handler);
        }
        private void ThrowExceptionIfDone()
        {
            if (isDone)
            {
                throw new BindAlreadyFinalizedException("The builder is already finalized.");
            }
        }
    }
}
